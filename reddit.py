#Reddit Crawler
import lxml
import requests
import sqlite3
import feedparser

database = "reddit.db"
URL = "https://www.reddit.com/r/opendirectories/.rss"
REDDITFEED = feedparser.parse(URL)

db = sqlite3.connect(database)
try:
	db.execute("SELECT * FROM directories")
except sqlite3.OperationalError:
	db.execute('''CREATE TABLE "directories" (
	`link`	TEXT NOT NULL UNIQUE,
	`name`	TEXT NOT NULL,
	`updated`	TEXT
	)''')

for post in REDDITFEED.entries:
	link = post.link
	name = post.title
	updated = post.updated
	try:
		tag = name.split("]")[0].split("[")[1]
		name = name.split("]")[1]
		if name.startswith(' '):
			name = name[1:]
		try:
			db.execute("INSERT INTO directories (link, name, updated, tag) VALUES(?, ?, ?, ?);", (link, name, updated, tag))
			print("Adding: " + name)
			db.commit()
		except sqlite3.IntegrityError:
			print("Exists: " + name)
		except Exception as e:
			print("Database Error: " + str(e))
	except IndexError:
		try:
			db.execute("INSERT INTO directories (link, name, updated) VALUES(?, ?, ?);", (link, name, updated))
			print("Adding: " + name)
			db.commit()
		except sqlite3.IntegrityError:
			print("Exists: " + name)
		except Exception as e:
			print("Database Error: " + str(e))
	except sqlite3.IntegrityError:
		print("Exists: " + name)
db.close()