import time
import feedparser
from lxml import html
from lxml import etree
import requests
import sqlite3
SECS = 1
INTERNALIP = "http://192.168.1.7:9117"
EXTERNALAD = "https://17thstreet.me/jackett"
MODE = "Internal"
if MODE == "Internal":
	IP = INTERNALIP
else:
	IP = EXTERNALAD
database = "magnet.db"
APIKEY = "fu7bry62eaisdhd2j7vqcqca5troylt6"
BASEURL = "http://192.168.1.7:9117"
print("Starting Yify")
YIFYFEED = feedparser.parse('https://yts.am/rss/0/1080p/all/0')
db = sqlite3.connect(database)
try:
	db.execute("SELECT * FROM magnets")
except sqlite3.OperationalError:
	db.execute('''CREATE TABLE "magnets" (
	`magnet`	TEXT NOT NULL UNIQUE,
	`name`	TEXT NOT NULL,
	`year`	TEXT,
	`genre`	TEXT,
	`image`	TEXT,
	`category`	TEXT NOT NULL,
	`site`	TEXT NOT NULL
	)''')
for post in YIFYFEED.entries:
	try:
		#Original feed dump: print '\r' + post.title + ": " + post.link + '\r'
		movieurl = requests.get(post.link)
		tree = html.fromstring(movieurl.content)
		name = tree.xpath("/html/body[@class='non-touch']/div[@class='main-content']/div[@id='movie-content']/div[@class='row'][1]/div[@id='movie-info']/div[@class='hidden-xs']/h1")
		year = tree.xpath("/html/body[@class='non-touch']/div[@class='main-content']/div[@id='movie-content']/div[@class='row'][1]/div[@id='movie-info']/div[@class='hidden-xs']/h2[1]")
		image = tree.xpath("/html/body[@class='non-touch']/div[@class='main-content']/div[@id='movie-content']/div[@class='row'][1]/div[@id='movie-poster']/img[@class='img-responsive']/@src")
		genre = tree.xpath("/html/body[@class='non-touch']/div[@class='main-content']/div[@id='movie-content']/div[@class='row'][1]/div[@id='movie-info']/div[@class='hidden-xs']/h2[2]")
		magnet = tree.xpath("/html/body//a[contains(@href,'1080p')][1]/@href[1]")
		#Variable Dump:
		name = name[0].text
		year = year[0].text
		image = image[0]
		genre = genre[0].text
		magnet = magnet[0]
		site = "YIFY"
		category = "MOVIES"
		try:
			db.execute("INSERT INTO magnets (magnet, name, year, genre, image, category, site) VALUES(?, ?, ?, ?, ?, ?, ?);", (magnet, name, year, genre, image, category, site))
			print("Adding: " + name)
			db.commit()
			time.sleep(SECS)
		except sqlite3.IntegrityError:
			print("Exists: " + name)
		except Exception as e:
			print("Database Error: " + str(e))
	except sqlite3.IntegrityError:
			print("Exists: " + name)
db.close()
print("Starting Rarbg")
RARMFEED = feedparser.parse('https://rarbg.to/rssdd.php?category=44') #_magnet.php?category
db = sqlite3.connect(database)
for post in RARMFEED.entries:
	try:
		magnet = post.link
		name = post.title
		category = "MOVIE"
		site = "RARBG"
		try:
			db.execute("INSERT INTO magnets (magnet, name, category, site) VALUES(?, ?, ?, ?);", (magnet, name, category, site))
			print("Adding: " + name)
			db.commit()
			time.sleep(SECS)
		except sqlite3.IntegrityError:
			print("Exists: " + name)
		except Exception as e:
			print("Error writing to database: " + str(e))
	except sqlite3.IntegrityError:
		print("Exists: " + name)
db.close()
##########################################################################################################################################
#Functions
##########################################################################################################################################
def Jackett(URL, SITE, CATEGORY):
	time.sleep(5)
	feed = feedparser.parse(URL)
	db = sqlite3.connect(database)
	print("Starting " + SITE)
	for post in feed.entries:
		name = post.title
		try:
			magnet = requests.get(post.link)
		except Exception as e:
			magnet = str(e) 
			newmagnet = magnet.split('magnet:')[1].split("'")[0]
			magnet = newmagnet
		try:
			db.execute("INSERT INTO magnets (magnet, name, category, site) VALUES(?, ?, ?, ?);", (magnet, name, CATEGORY, SITE))
			print("Adding: " + name)
			db.commit()
			time.sleep(SECS)
		except sqlite3.IntegrityError:
			print("Exists: " + name)
		except Exception as e:
			print("Error: " + str(e))
	db.close()

Jackett(IP + "/api/v2.0/indexers/torrentdownloads/results/torznab/api?apikey=fu7bry62eaisdhd2j7vqcqca5troylt6&t=search&cat=100004&q=", "torrentdownloads", "MOVIES")
#Jackett(IP + "/api/v2.0/indexers/kickasstorrent/results/torznab/api?apikey=fu7bry62eaisdhd2j7vqcqca5troylt6&t=search&cat=100071&q=", "Kickass", "MOVIES")
#Jackett(IP + "/api/v2.0/indexers/demonoid/results/torznab/api?apikey=fu7bry62eaisdhd2j7vqcqca5troylt6&t=search&cat=100001&q=", "Demonoid", "MOVIES")
Jackett(IP + "/api/v2.0/indexers/kickasstorrent-kathow/results/torznab/api?apikey=fu7bry62eaisdhd2j7vqcqca5troylt6&t=search&cat=2000&q=", "Kat.li", "MOVIES")
Jackett(IP + "/api/v2.0/indexers/ettv/results/torznab/api?apikey=fu7bry62eaisdhd2j7vqcqca5troylt6&t=search&cat=100001&q=", "ETTV", "MOVIES")
Jackett(IP + "/api/v2.0/indexers/isohunt2/results/torznab/api?apikey=fu7bry62eaisdhd2j7vqcqca5troylt6&t=search&cat=100005&q=", "Isohunt2", "MOVIES")


#Let's put some happy little TV down here
Jackett("http://192.168.1.7:9117/api/v2.0/indexers/eztv/results/torznab/api?apikey=fu7bry62eaisdhd2j7vqcqca5troylt6&t=search&cat=5000&q=", "EZTV", "TV")
db.close()
