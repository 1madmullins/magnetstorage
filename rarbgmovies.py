import feedparser
from lxml import html
from lxml import etree
import requests
import sqlite3
database = "magnet.db"
RARMFEED = feedparser.parse('https://rarbg.to/rssdd.php?category=44') #_magnet.php?category
db = sqlite3.connect(database)
for post in RARMFEED.entries:
	try:
		#print('\r' + post.title + ": " + post.link + '\r') #
		#movieurl = requests.get(post.link)
		#tree = html.fromstring(movieurl.content)
		#name = tree.xpath("/html/body[@class='non-touch']/div[@class='main-content']/div[@id='movie-content']/div[@class='row'][1]/div[@id='movie-info']/div[@class='hidden-xs']/h1")
		#year = tree.xpath("/html/body[@class='non-touch']/div[@class='main-content']/div[@id='movie-content']/div[@class='row'][1]/div[@id='movie-info']/div[@class='hidden-xs']/h2[1]")
		#image = tree.xpath("/html/body[@class='non-touch']/div[@class='main-content']/div[@id='movie-content']/div[@class='row'][1]/div[@id='movie-poster']/img[@class='img-responsive']/@src")
		#genre = tree.xpath("/html/body[@class='non-touch']/div[@class='main-content']/div[@id='movie-content']/div[@class='row'][1]/div[@id='movie-info']/div[@class='hidden-xs']/h2[2]")
		magnet = post.link
		#Variable Dump:
		name = post.title
		#year = year[0].text
		#image = image[0]
		#genre = genre[0].text
		#magnet = magnet[0]
		category = "MOVIE"
		site = "RARBG"
		db.execute("INSERT INTO magnets (magnet, name, category, site) VALUES(?, ?, ?, ?);", (magnet, name, category, site))
		db.commit()
	except sqlite3.IntegrityError:
		print(name + " Exists in DB")
db.close()